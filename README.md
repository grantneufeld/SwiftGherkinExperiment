# SwiftGherkinRunner

Trying out some ideas for an implementation of Gherkin-based feature testing in Swift.

May, 2021, by Grant Neufeld.

⚠️ This is incomplete and doesn’t work yet.
Go through the code and find the “⚠️” comments to see where stuff still needs to be done.

## Usage

This is currently designed to run as part of the UITests in an Xcode project.

### Setup

Include the `SwiftGherkinRunner` library somewhere in your project.
Make sure all the files in the library have Xcode “Target Membership”
pointing at your “MyAppUITests” target.

Within the top level of your project’s UITests folder (e.g, `MyAppUITests/`),
add the folders `Features` and `GherkinMappers`.

Add the `FeatureRunnerUITests.swift` file in the top of your UITests folder.

```swift
// FeatureRunnerUITests.swift
import XCTest

class FeatureRunnerUITests: XCTestCase {
    func testFeatures() throws {
        FeatureRunner.run()
    }
}
```

(That’s the file that causes your features to be run when your UI tests are run.)

### Feature Files

You define your features using Gherkin syntax (as described in this document)
in files with names ending with `.feature`.
These files must go in the `Features` folder.

### Mappers

In order to be able to run the features/scenarios defined in the feature files,
there need to be mappers defined.

Mappers must be classes that inherit from `GherkinMapper`,
with names ending in `Mapper` (e.g., `class UserAccountMapper: GherkinMapper`).
You can have one or more of these classes.
Each must be in its own file, with the filename matching the class name
(e.g., `UserAccountMapper.swift`).
Those files must be in the `GherkinMappers` folder.
You will also need to ensure that they are included when compiling & building the UI Tests
(Xcode Target Membership: MyAppUITests).

#### Initialization

In the Mapper subclass, you must override the void init call (`override init() {}`)
to map text strings to custom functions to call.
This is what enables the use of plain text syntax in the Gherkin scenario actions (“Given”, “When”, “Then”)
to trigger code actions.

Within that init call, you can use calls to `given(String, method)`, `when(String, method)`,
and `then(String, method)`, to map text to specific methods you define.

#### Methods

The custom methods mapped by the Mappers for use in scenario actions must have the format
`(GherkinExample?) throws -> Bool` (type-aliased as `GherkinMapperMethod`).

The `GherkinExample` object that may be passed in will contain the values
from an “Examples:” table row for the scenario.
The values are accessed using the subscript operator (just like a Dictionary container).

Example:

```gherkin
| Item | Count |
| Box | 13 |
```

```swift
func myMethod(_ example: GherkinExample?) throws -> Bool
```

means that your method can call `example["Item"]` and get `"Box"`,
and `example["Count"]` will get `"13"`.

Note that all example values are strings. So, you’re responsible for any conversions to other types
(e.g., `let itemCount = Int(example["Count"])` would get `13` as an integer instead of string,
in the above example).

The `Bool` returned by the method will be ignored for methods called by “Given” or “When” actions.
It is only applicable when testing the results of a scenario in “Then” actions.
For that, `true` means the test passed, and `false` means it failed.

Throwing an error (exception) will cause the scenario to fail,
but will not halt subsequent scenarios and feature from running.

Your methods can make use of any of the calls you would normally make in a UITest `test` method.

#### Example Mapper

```swift
import XCTest

/// Implementation of Gherkin mappings for MyApp.
final class MyAppMapper: GherkinMapper {
    override init() {
        super.init()
        given("I am a registered user", iAmRegistered)
    }

    func iAmRegistered(_ example: GherkinExample? = nil) throws {
        // setup a registered user to use...
    }
}
```

## Gherkin Syntax Support

Here are the specifics of what elements of the Gherkin syntax this software supports,
and how it interprets them.

### Spacing

There may be any number of blank lines between statements. These are ignored.

Only Feature and Scenario/Scenario Outline descriptions may span multiple lines.
Any other statements that are split across multiple lines will result in an error.
When spanning multiple lines,
individual lines of those descriptions must not start with any of the line openers defined in this syntax.

Lines can have any amount of space characters at the start—indentation does not affect processing.
Tab characters are not supported.

The opening of a statement may be followed by one or more spaces to separate it from the content.
E.g., `When         I ignore extra space before the content`
gives a “When” action of `"I ignore extra space before the content"`.

However, extra spaces *within* content will not be trimmed.
So, `When I   have too   much space` results in
`I   have too   much space` as the action content.

### `Feature:`

A feature describes a particular set of functionality.
It may include some further descriptive elements
(`In order` that an outcome happens, `As a` role, `I want` to do something.).
It should include one or more Scenarios describing specific expectations for the feature.

All features must begin with a line that starts with `Feature:`.
The line may continue with plain text to describe/name the feature.
This plain text description may span over multiple lines.

There may be any number of features in an individual file,
but it can be good practice to have just one per file.

Features may start anywhere in a file,
and mark the closure of any prior feature/scenario in the file.

Example:

```gherkin
Feature: Demonstrate features
         using Gherkin Syntax.
```

### `In order`

The “In order” line describes the desired outcome for the feature.

A single `In order` line may occur for a feature (but must be before any scenarios for the feature).

It should also precede the `As a` and `I want` lines, if they are present.

Example: `In order to illustrate how features work,`.

### `As a`, `As an`

The “As a” (or “As an”) line describes who the feature is for—what role the user of the feature has.

A single `As a` line may occur for a feature (but must be before any scenarios for the feature).

It may also be written as `As an`, if that is grammatically better.

It should follow the `In order` line, and precede the `I want` line, if they are present.

Example: `As a software documenter,`

### `I want`

The “I want” line describes what the user will do with the feature.

A single `I want` line may occur for a feature (but must be before any scenarios for the feature).

It should follow the `In order` line, and the `As a` line, if they are present.

Example: `I want to provide clear sample code.`

### `Scenario:` and `Scenario Outline:`

A scenario describes a specific, individual, test.

It should consist of zero or more `Given`s,
one or more `When`s,
and one or more `Then`s.

All scenarios must begin with a line that starts with `Scenario:` or `Scenario Outline:`.
The line may continue with plain text to describe/name the scenario.
This plain text description may span over multiple lines.

Scenarios can start anywhere within a Feature,
and mark the closure of the prior Scenario, if there is one.

There may be any number of scenarios for an individual feature.
However, if you end up with a lot, you may want to consider splitting the feature
into separate, more specific, features.

Example:

```gherkin
Scenario: Have a Scenario
          within a Feature.
```

### `Scenario Outline:`

`Scenario Outline:` are identical to `Scenario:` lines.

The difference is that scenario outlines is intended to be used
when including `Examples:` with a scenario.

### `Given`, `When`, `Then`: Scenario actions

Scenarios consist of some `Given`, `When`, and `Then` lines (and sometimes `And` or `But` lines).
Each of these lines defines an action to be performed when the scenario is run.

All of the givens in a scenario must precede the whens, which must precede the thens.

If there is more than one of a type of action in a scenario,
you can use an `And` or `But` line for subsequent lines after the first.

Example:

```gherkin
Given I have more than one given
  And I have enough room to illustrate this
  But I don’t have anything else for this example
```

The action content (the text that follows, but does not include, the line opener)
has to be mapped by `GherkinMapper` sub-classes to specific methods to call.
You can use any single-line text for the action content,
but you will have to ensure it is mapped by a `GherkinMapper` sub-class.

The action content may contain sub-elements
defined by portions of the text wrapped either by quotes (`"` or `'`),
or by angle brackets (`<...>`).
If there are `Examples` provided with the scenario,
those sub-elements will be replaced with the corresponding example data.
If there is no example data for the scenario, the sub-elements will be left as-is.

Example: `When I fill in the form with "my name"`".
If there is a `my name` value in examples included with the scenario,
the example data will effectively replace `my name` when the scenario runs.
(e.g., example data `my name:Foo Bar`
effectively turns the example into `I fill in the form with Foo Bar`)

#### `Given`

Describes a pre-condition for a scenario.

This is where you setup any data or software state that should be in place
to be able to perform the scenario “When” actions to get the “Then” outcomes desired.

Example:

```gherkin
Given there are 5 items in my shopping cart
  And there are sufficient funds available on my card
```

#### `When`

Describes the user/system actions to be performed for the scenario.

This is where you put the actions that the user (or system) should perform in the scenario.

Example: `When I do the checkout of my shopping cart`

#### `Then`

Describes the outcomes expected from performing the “When” actions in the scenario.

Example:

```gherkin
Then I should see an invoice for 5 items`
 And I should see a delivery date 2 days from now
```

### `Examples:`

Opens a table of values to be used when running a scenario.

Examples do not have any description, so there should not be any text after `Examples:` on the line.

There can only be one set of examples for a scenario.

### Example table rows `|`

Table rows consist one or more values wrapped in pipe characters (`|`).
Any space characters on either side of a pipe character will be ignored.

The first row defines the labels to use for the values on subsequent rows.

There is no limit to the number of rows in the table.

Example:

```gherkin
Examples:
| ID | Name |
|  1 | Bobby|
|1234| Alfonse |
```

In that example, there are two example rows that define the data values
“ID:1,Name:Bobby” and “ID:1234,Name:Alfonse”.

If the scenario includes a line like `Given a record ID "ID", named "Name"`,
that will effectively become `Given a record ID 1, named Bobby`
and `Given a record id 1234, named Alfonse`.

### `#` Comments

Lines starting with `#` are ignored (treated as blank lines).

Example: `# a comment line to be ignored`

### `@` Tags

Tags can be used to exclude Features or individual Scenarios from being run,
or to limit a run to only Features or Scenarios matching a tag.
Basically, they can be used to define a subset of features and scenarios that can be acted on or excluded.

Tags are lines starting with a `@` symbol.
One tag per line.
Tags may only precede `Feature` or `Scenario`/`Scenario Outline` lines—or other tag lines.

Example:

```gherkin
@FirstTag
@AnotherTag
Feature: Tagging features.
```

## Some Good Practices for Writing Features

Features describe the desired **behaviours** of the system, not how it is built.
They should not describe specific system implementation details—leave that for unit or integration tests.

Features should generally lean toward high-level, business domain, descriptions.

E.g., “When I login as Bob” instead of “When I click on the Login button,
And I enter "bob" in the username field, And I enter "MyPass" in the password field, …”

In general, it’s best to minimize the use of “And” (and “But”) lines when defining actions in a scenario.
Having more than a few in one scenario can be considered a code smell that may suggest
you’re either getting too close to implementation details,
or that the scenario should be split into multiple scenarios.

Features and scenarios should be written to be readily understood by non-programmer folks
who understand the business domain.
Domain specific jargon can be fine to use, but coding-specific terms should be avoided.

## Similar or Related Projects

* [SwiftGherkin](https://github.com/iainsmith/SwiftGherkin)
* [TABTestKit](https://github.com/theappbusiness/TABTestKit/)
* [Ploughman](https://github.com/kylef-archive/Ploughman)
* [XCFit](https://github.com/XCTEQ/XCFit)
* [XCTest-Gherkin](https://github.com/net-a-porter-mobile/XCTest-Gherkin)
