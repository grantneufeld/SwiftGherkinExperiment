import Foundation

/// Iterator for FeatureFiles so they can be used in `for` loops.
/// Iterates over the lines in the file.
struct FeatureFileIterator: IteratorProtocol {
    let file: FeatureFile
    private let lines: [String.SubSequence]
    private var index = 0

    /// IteratorProtocol: the type of element in the collection
    typealias Element = String

    init(file: FeatureFile) {
        self.file = file
        do {
            // load the file data into individual lines
            let contents = try String(contentsOfFile: file.filePath)
            self.lines = contents.split(separator: "\n")
        } catch {
            self.lines = []
        }
    }

    /// IteratorProtocol: Get the next element.
    mutating func next() -> String? {
        if index < lines.count {
            let line = lines[index]
            index += 1
            return String(line)
        } else {
            return nil
        }
    }
}
