import Foundation

/// Parse the feature files in the features directory.
public enum GherkinLoader {
    /// Parse each file in the features directory into a GherkinFeature.
    static func loadFeatures(reportTo: MessageReporter) throws -> [GherkinFeature] {
        var features: [GherkinFeature] = []
        // ⚠️ how do we find the Features directory?
        let featuresDirectoryFiles: [FeatureFile] = [] // ⚠️ getDirectory("features") // •
        for featureFile in featuresDirectoryFiles {
            let parser = GherkinFileParser(file: featureFile, reportTo: reportTo)
            features += parser.features
        }
        return features
    }

    /// Load the custom gherkin-string to function call mappers.
    static func loadMappers() -> [GherkinMapper] {
        let mappers: [GherkinMapper] = []
        // ⚠️ how do we find and load all the custom sub-classes of GherkinMapper???
        // get the mappers directory(ies)
        // go through each file in the directory
            // if the filename ends with "Mapper.swift"
                // add it to our collection
                //    let mapperClass = ???
                //    mappers.add(mapperClass.new)
        return mappers
    }
}
