import Foundation

/// A Gherkin format Feature file (`.feature`).
struct FeatureFile: Hashable, Sequence {
    let filePath: String
    var filename: String {
        filePath
    }

    /// Iterate over the lines in the file.
    /// - Returns: an Iterator
    func makeIterator() -> FeatureFileIterator {
        FeatureFileIterator(file: self)
    }
}
