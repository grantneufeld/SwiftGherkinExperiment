import Foundation

// swiftlint:disable type_body_length

/// Turn a Gherkin Feature text file into feature objects.
/// Access the resulting features through the `features` attribute.
final class GherkinFileParser {
    static let lineOpeners = [
        "Feature:", "In order", "As a", "As an", "I want",
        "Scenario:", "Scenario Outline:",
        "Given", "When", "Then", "And", "But",
        "Examples:", "\\|",
        "@", "#"
    ]
    /// The Gherkin format file to be parsed.
    let file: FeatureFile
    private let report: MessageReporter
    /// Features found within the file.
    var features: [GherkinFeature] = []
    /// The number of extra clauses (e.g., “And” or “But” lines) allowed for any part of a scenario.
    /// 0 means no limit.
    var andLimit: Int = 4

    private enum State: String {
        case none
        case feature
        case inOrder
        case asA
        case iWant
        case scenario
        case given
        case when
        case then
        case examples

        var asString: String {
            switch self {
            case .none:
                return "<undefined>"

            case .inOrder:
                return "In order"

            case .asA:
                return "As a"

            case .iWant:
                return "I want"

            default:
                return rawValue
            }
        }
    }

    private var state = State.none
    private var currentLineNumber: Int = 0
    private var tags: Set<GherkinTag> = []
    private var lineType: String?
    private var content: String = ""
    private var currentFeature: GherkinFeature?
    private var currentScenario: GherkinScenario?
    /// The number of lines in a Given, When, or Then, group.
    private var linesInChunk: Int = 0
    private var exampleColumns: [String] = []
    // private var examples: [GherkinExample] = []

    // MARK: - Parsing

    /// Initialize a parser for Gherkin format feature files.
    /// - Parameters:
    ///   - file: The FeatureFile to be parsed.
    ///   - reportTo: MessageReporter that will receive messages about errors encountered while parsing.
    init(file: FeatureFile, reportTo: MessageReporter) {
        self.report = reportTo
        self.file = file
        for line in file {
            self.currentLineNumber += 1
            parseLine(line)
        }
    }

    /// Parse a line from a Gherkin format feature file.
    /// - Parameters:
    ///   - gherkinLine: The text of the line from a Gherkin file.
    // swiftlint:disable:next cyclomatic_complexity
    private func parseLine(_ gherkinLine: String) {
        (lineType, content) = categorizeGherkinLine(gherkinLine)

        switch lineType {
        case "@":
            tagLine()

        case "Feature":
            featureLine()

        case "In order":
            inOrderLine()

        case "As a", "As an":
            asALine()

        case "I want":
            iWantLine()

        case "Scenario":
            scenarioLine()

        case "Given":
            givenLine()

        case "When":
            whenLine()

        case "Then":
            thenLine()

        case "And", "But":
            andOrButLine()

        case "Examples":
            examplesLine()

        case "|":
            tableLine()

        case "", "#":
            // don’t do anything for blank lines or comments
            break

        default:
            plainTextLine()
        }
    }

    /// Determine the type of Gherkin line, and split out the content.
    /// A `nil` lineType means an unrecognized plain text line.
    /// An empty string lineType means a blank line.
    /// - Parameter gherkin: line from a Gherkin format file.
    /// - Returns: lineType: String?, content: String
    private func categorizeGherkinLine(_ gherkin: String) -> (String?, String) {
        if gherkin.isEmpty {
            // blank line
            return ("", "")
        }
        // Unless it’s a plain text line,
        // the first part is the opening of the line, ignoring leading white-space,
        // the second part is the line content, minus the opening and white-space separating them.
        let pattern = "^ *(\(Self.lineOpeners.joined(separator: "|"))) *(.*)$"
        var parts: [String] = []
        do {
            parts = try gherkin.captureGroups(pattern: pattern)
        } catch {
            addError("Regular expression error encountered.")
        }
        if parts.count == 2 {
            return (parts[0], parts[1])
        } else {
            return (nil, gherkin.trimmingCharacters(in: .whitespaces))
        }
    }

    func tagLine() {
        self.tags.insert(GherkinTag(file: file, lineNumber: currentLineNumber, name: content))
    }

    private func featureLine() {
        state = .feature

        self.currentFeature = GherkinFeature(file: file, lineNumber: currentLineNumber, title: content, tags: tags)
        self.tags = []
        features.append(currentFeature!)
    }

    private func inOrderLine() {
        if state != .feature {
            addError("“In order” line found out of place. Must follow a “Feature” line.")
        }
        state = .inOrder
        currentFeature?.inOrder = content
    }

    private func asALine() {
        if state != .inOrder {
            addError("“In order” line found out of place. Must follow an “In order” line.")
        }
        state = .asA
        currentFeature?.asA = content
    }

    private func iWantLine() {
        if state != .feature {
            addError("“I want” line found out of place. Must follow an “As a” line.")
        }
        state = .iWant
        currentFeature?.iWant = content
    }

    private func scenarioLine() {
        state = .scenario
        self.currentScenario = GherkinScenario(
            file: file, lineNumber: currentLineNumber, description: content, tags: tags
        )
        self.tags = []
        currentFeature?.append(currentScenario!)
    }

    private func givenLine() {
        if state != .scenario && state != .given {
            addError(
                "“Given” line found out of place. Must be at the start of a Scenario, or following another Given."
            )
        }
        if state == .given {
            linesInChunk += 1
        } else {
            linesInChunk = 0
        }
        state = .given
        currentScenario?.addAction(content, lineNumber: currentLineNumber)
    }

    private func whenLine() {
        if state != .scenario && state != .given && state != .when {
            addError(
                "“When” line found out of place. Must follow a Given, or When, or be at the start of a Scenario."
            )
        }
        if state == .when {
            linesInChunk += 1
        } else {
            linesInChunk = 0
        }
        state = .when
        currentScenario?.addAction(content, lineNumber: currentLineNumber)
    }

    private func thenLine() {
        if state != .when && state != .then {
            addError("“Then” line found out of place. Must follow a When, or another Then.")
        }
        if state == .then {
            linesInChunk += 1
        } else {
            linesInChunk = 0
        }
        state = .then
        currentScenario?.addAction(content, lineNumber: currentLineNumber)
    }

    private func andOrButLine() {
        linesInChunk += 1
        if andLimit > 0 && linesInChunk >= andLimit {
            addError("Too many statements for “\(state.asString)” in Scenario.")
        }
        switch state {
        case .given, .when, .then:
            currentScenario?.addAction(content, lineNumber: currentLineNumber)

        default:
            addError("“\(lineType!)” line found out of place. Must follow a Given, When, or Then.")
        }
    }

    private func examplesLine() {
        if state != .then && state != .examples {
            addError("Examples found out of place. Must follow a “Then” line.")
        }
        state = .examples
        // reset the column labels
        exampleColumns = []
    }

    /// Process a line from an examples table where the cells are separated by `|`.
    /// Expects the content to not start with a `|`.
    private func tableLine() {
        if state != .examples {
            addError("Example table line found out of place. Must be within an Examples block.")
        }
        // strip trailing `|`, then split based using `|` as separator (omitting extra white-space)
        let values = content.gsub(pattern: #" *\|$"#, with: "").split(pattern: #" *\| *"#)
        if exampleColumns.isEmpty {
            tableFirstLine(values: values)
        } else {
            tableExampleLine(values: values)
        }
    }

    /// Set the column header for the current Examples table.
    /// - Parameter values: array of labels (Strings) to use for the columns in the table
    private func tableFirstLine(values: [String]) {
        exampleColumns = values
    }

    /// Add an example from an Examples table.
    /// - Parameter values: Array of values (Strings) for the cells in the row
    private func tableExampleLine(values: [String]) {
        guard values.count == exampleColumns.count else {
            addError(
                """
                Number of columns in this row (\(values.count)) does not match
                the number of columns in the table header (\(exampleColumns.count)).
                """
            )
            return
        }
        let example = GherkinExample(file: file, lineNumber: currentLineNumber)
        for index in 0..<(exampleColumns.count) {
            let label = exampleColumns[index]
            let value = values[index]
            example[label] = value
        }
        currentScenario?.addExample(example)
    }

    /// Handle parsing a line that doesn’t have a defined opener.
    private func plainTextLine() {
        switch state {
        case .feature:
            currentFeature?.title = appendLine(currentFeature?.title, content)

        case .inOrder:
            currentFeature?.inOrder = appendLine(currentFeature?.inOrder, content)

        case .asA:
            currentFeature?.asA = appendLine(currentFeature?.asA, content)

        case .iWant:
            currentFeature?.iWant = appendLine(currentFeature?.iWant, content)

        case .scenario:
            currentScenario?.description = appendLine(currentScenario?.description, content)

        default:
            addError(
                "Plain text line found out of place. Must follow a Feature or Scenario line."
            )
        }
    }

    // MARK: - Helpers

    /// Join two strings, using a newline separator if both are not nil.
    private func appendLine(_ existing: String?, _ addition: String?) -> String {
        var line = ""
        if existing != nil {
            line = "\(existing!)\n\(addition ?? "")"
        } else {
            line = addition ?? ""
        }
        return line
    }

    private func addError(_ text: String) {
        report.error(file: file, lineNumber: currentLineNumber, content: text, example: nil)
    }
}
