import Foundation

/// Wrapper around all the things that need to be done to load and run the features
/// in the current Xcode project.
public enum FeatureRunner {
    static func run(reportTo: MessageReporter = StdoutXcodeReporter()) throws {
        let features: [GherkinFeature] = try GherkinLoader.loadFeatures(reportTo: reportTo)
        for feature in features {
            // ⚠️ support restricting which features/scenarios run based on passing in tags
            feature.run(reportTo: reportTo)
        }
    }
}
