import Foundation

/// Map Gherkin strings to function calls.
/// Abstract class to be inherited from in the projects that use this testing tool.
public class GherkinMapper {
    typealias GherkinActionMethod = (GherkinExample?) throws -> Bool
    /// Methods that can be called for every Scenario run,
    /// before or after the given/when/then actions.
    typealias GherkinEveryTimeMethod = () throws -> Void

    private static var beforeActions: [GherkinEveryTimeMethod] = []
    private static var actions: [String: GherkinActionMethod] = [:]
    private static var afterActions: [GherkinEveryTimeMethod] = []

    // MARK: - Setup Mappers

    /// Add an action to perform before each Scenario is run.
    /// - Parameter call: the method to call.
    func before(_ call: @escaping GherkinEveryTimeMethod) {
        Self.beforeActions.append(call)
    }

    /// Add an action to perform after each Scenario is run.
    /// - Parameter call: the method to call.
    func after(_ call: @escaping GherkinEveryTimeMethod) {
        Self.afterActions.append(call)
    }

    /// Add a “Given” mapper for a gherkin format string.
    /// - Parameters:
    ///   - gherkin: the Gherkin format text to match
    ///   - call: the function to call (the function must take an array of strings)
    func given(_ gherkin: String, _ call: @escaping GherkinActionMethod) {
        Self.actions[gherkin] = call
    }

    /// Add a “When” mapper for a gherkin format string.
    /// - Parameters:
    ///   - gherkin: the Gherkin format text to match
    ///   - call: the function to call (the function must take an array of strings)
    func when(_ gherkin: String, _ call: @escaping GherkinActionMethod) {
        Self.actions[gherkin] = call
    }

    /// Add a “Then” mapper for a gherkin format string.
    /// - Parameters:
    ///   - gherkin: the Gherkin format text to match
    ///   - call: the function to call (the function must take an array of strings)
    func then(_ gherkin: String, _ call: @escaping GherkinActionMethod) {
        Self.actions[gherkin] = call
    }

    // MARK: - Check For Mappers

    /// Determine whether there is a mapper for the Gherkin format text to a method.
    /// - Parameter gherkin: the Gherkin format text to match
    /// - Returns: true if there is a mapper
    static func hasAction(_ gherkin: String) -> Bool {
        Self.actions[gherkin] != nil
    }

    // MARK: - Perform Methods Using Mappers

    /// Perform all the actions that have been assigned to run before each Scenario.
    /// - Throws: whatever errors might be thrown by the assigned methods.
    static func runBefore() throws {
        for action in beforeActions {
            try action()
        }
    }

    /// Perform all the actions that have been assigned to run after each Scenario.
    /// - Throws: whatever errors might be thrown by the assigned methods.
    static func runAfter() throws {
        for action in afterActions {
            try action()
        }
    }

    // ⚠️ parse substrings (quote-wrapped) to match `<1>`, `<2>`, etc., in givens/whens/thens
    //  then pass those as the values to the called method

    /// Call the method matching the specified Gherkin format text.
    /// - Parameters:
    ///   - gherkin: the Gherkin format text to match
    ///   - example: a GherkinExample with values to use
    /// - Throws: `GherkinError.missingMapper` if there is not matching mapper
    /// - Returns: true if the action succeeded
    static func handleAction(_ gherkin: String, example: GherkinExample? = nil) throws -> Bool {
        if let method = actions[gherkin] {
            return try method(example)
        } else {
            throw GherkinError.missingMapper
        }
    }
}
