import Foundation

/// Details of an error encountered while processing a Scenario.
struct ScenarioError {
    /// The file that the error was encountered in.
    let file: FeatureFile
    /// The applicable line number within the feature file.
    let lineNumber: Int
    /// The content of the line in the scenario where the error occurred.
    let content: String
    /// The values used in an example for a scenario.
    let example: GherkinExample?
    /// Whether the error is just a warning, a test failure, or a regular error.
    let category: ReportCategory

    init(
        file: FeatureFile,
        lineNumber: Int,
        content: String,
        example: GherkinExample? = nil,
        category: ReportCategory = .error
    ) {
        self.file = file
        self.lineNumber = lineNumber
        self.content = content
        self.example = example
        self.category = category
    }

    var report: String {
        "\(file.filename):\(lineNumber): Scenario test failed: \"\(content)\"\(exampleValues)"
    }

    var xcode: String {
        "\(file.filePath):\(lineNumber):1: \(xcodeCategory): \"\(content)\"\(exampleValues)"
    }

    private var xcodeCategory: String {
        // ⚠️ how do we mark an entry as a successful test when reporting back to Xcode?
        if category == .warning {
            return "warning"
        } else {
            return "error"
        }
    }

    private var exampleValues: String {
        if let example = example {
            return "; example parameters: \(example.toString())"
        } else {
            return ""
        }
    }
}
