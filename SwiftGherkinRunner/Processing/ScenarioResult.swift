import Foundation

/// Collection of results from processing a scenario.
/// Will be empty if there were no errors.
final class ScenarioResult {
    var errors: [ScenarioError] = []
    var count: Int {
        errors.count
    }
    var isEmpty: Bool {
        errors.isEmpty
    }

    func append(_ error: ScenarioError) {
        errors.append(error)
    }

    /// Add a ScenarioError to the results.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    func addError(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample? = nil) {
        let error = ScenarioError(
            file: file, lineNumber: lineNumber, content: content, example: example
        )
        append(error)
    }
}
