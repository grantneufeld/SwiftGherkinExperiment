import Foundation

/// An action line (Given, When, Then), for a Scenario, in a Gherkin format Feature file.
struct GherkinAction {
    let file: FeatureFile
    let lineNumber: Int
    let lineType: String // Given, When, Then
    let content: String
}
