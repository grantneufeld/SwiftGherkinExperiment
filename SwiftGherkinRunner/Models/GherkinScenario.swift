import Foundation
import XCTest

/// An individual scenario, within a feature, that can be tested.
/// Use the `run` method to perform the scenario test.
final class GherkinScenario {
    let file: FeatureFile
    /// The line number the Scenario starts on in the file.
    let lineNumber: Int
    var tags: Set<GherkinTag>
    var description: String
    var actions: [GherkinAction] = []
    var examples: [GherkinExample] = []

    private var report: MessageReporter = StdoutXcodeReporter()

    // MARK: - Setup

    init(file: FeatureFile, lineNumber: Int, description: String, tags: Set<GherkinTag> = []) {
        self.file = file
        self.lineNumber = lineNumber
        self.description = description
        self.tags = tags
    }

    /// Add an action to the Scenario.
    /// - Parameters:
    ///   - text: the text to use
    ///   - lineNumber: the line number within the feature source file
    func addAction(_ text: String, lineNumber: Int) {
        let action = GherkinAction(file: file, lineNumber: lineNumber, lineType: "Then", content: text)
        actions.append(action)
    }

    /// Add an Example to use when running the Scenario.
    /// - Parameter example: a GherkinExample
    func addExample(_ example: GherkinExample) {
        examples.append(example)
    }

    // MARK: - Perform

    /// Perform the Scenario.
    /// - Parameters:
    ///   - example: Values to be used when running the scenario.
    ///   - reportTo: a MessageReporter to send errors and other messages to.
    func run(onlyTagged: Set<GherkinTag>, reportTo: MessageReporter) {
        self.report = reportTo
        guard canRunWithTags(onlyTagged: onlyTagged) else {
            return
        }
        guard hasRequiredMappers() else {
            return
        }
        // ⚠️ from XCTest: UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        //
        if examples.isEmpty {
            performScenarioLines()
        } else {
            for example in examples {
                performScenarioLines(example: example)
            }
        }
        // ⚠️ probably need to close/reset the app here after the scenario has run
    }

    private func canRunWithTags(onlyTagged: Set<GherkinTag>) -> Bool {
        onlyTagged.isEmpty || onlyTagged.isDisjoint(with: tags)
    }

    /// Check if all the required mappers are available.
    /// - Returns: true if all mappers are available, false if any mappers are missing
    private func hasRequiredMappers() -> Bool {
        var covered = true
        for action in actions {
            if !GherkinMapper.hasAction(action.content) {
                covered = false
                reportMissingMapper(action: action)
            }
        }
        return covered
    }

    /// Report that a required mapper is missing.
    /// - Parameters:
    ///   - action: the GherkinAction to be mapped
    private func reportMissingMapper(action: GherkinAction) {
        report.error(
            file: action.file,
            lineNumber: action.lineNumber,
            content: "missing required mappers for \"\(action.content)\"",
            example: nil
        )
    }

    /// Perform the individual Given, When, and Then, lines for the Scenario.
    /// - Parameter example: Values to be used when running the scenario.
    private func performScenarioLines(example: GherkinExample? = nil) {
        var lineNumber: Int = 0
        var content = ""
        do {
            for action in actions {
                lineNumber = action.lineNumber
                content = action.content
                let success = try GherkinMapper.handleAction(content, example: example)
                if !success {
                    report.failure(
                        file: file, lineNumber: lineNumber, content: "failed test: \(content)", example: example
                    )
                }
            }
        } catch GherkinError.missingMapper {
            report.error(
                file: file, lineNumber: lineNumber, content: "missing mapper for “\(content)”", example: example
            )
        } catch {
            fatalError("unrecognized exception thrown: '\(error)'")
        }
    }
}
