import Foundation

/// An individual feature with scenarios that can be tested.
/// Use the `run` method to perform all the scenario tests for the feature.
final class GherkinFeature {
    /// The file the feature is in.
    let file: FeatureFile
    /// The line number that the feature starts on in the file.
    let lineNumber: Int
    var tags: Set<GherkinTag> = []
    var title: String
    var inOrder: String?
    var asA: String?
    var iWant: String?
    private var scenarios: [GherkinScenario] = []

    init(file: FeatureFile, lineNumber: Int, title: String, tags: Set<GherkinTag>) {
        self.file = file
        self.lineNumber = lineNumber
        self.title = title
        self.tags = tags
    }

    /// Add a Scenario to the feature.
    /// - Parameter scenario: the Scenario to include in the Feature
    func append(_ scenario: GherkinScenario) {
        scenarios.append(scenario)
    }

    /// Run all the scenarios of the feature.
    /// - Parameters:
    ///   - onlyTagged: if there are tags specified, restrict the run to features/scenarios that have those tags.
    ///   - reportTo: a MessageReporter to send errors and other messages to.
    func run(onlyTagged: Set<GherkinTag> = [], reportTo report: MessageReporter = StdoutXcodeReporter()) {
        var passAlongTags: Set<GherkinTag> = []
        if canRunWithTags(onlyTagged: onlyTagged) {
            // All Scenarios can run if the Feature can run.
        } else {
            // Scenarios need one of the tags to run.
            passAlongTags = onlyTagged
        }
        for scenario in scenarios {
            scenario.run(onlyTagged: passAlongTags, reportTo: report)
        }
    }

    private func canRunWithTags(onlyTagged: Set<GherkinTag>) -> Bool {
        onlyTagged.isEmpty || onlyTagged.isDisjoint(with: tags)
    }
}
