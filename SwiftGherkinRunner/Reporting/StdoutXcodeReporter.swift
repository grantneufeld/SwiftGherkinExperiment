import Foundation

/// Reports messages on stdout in a format Xcode parses for warnings/errors.
struct StdoutXcodeReporter: MessageReporter {
    /// Report an error.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    func error(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample? = nil) {
        reportMessage(
            file: file, lineNumber: lineNumber, content: content, example: example, category: .error
        )
    }

    /// Report a warning.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    func warning(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample? = nil) {
        reportMessage(
            file: file, lineNumber: lineNumber, content: content, example: example, category: .warning
        )
    }

    /// Report a test success.
    /// - Parameters:
    ///   - file: the FeatureFile the success occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the success applies to
    ///   - example: if running as part of an example, the GherkinExample
    func success(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample?) {
        // ⚠️ don’t report passing tests
    }

    /// Report a test failure.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    func failure(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample? = nil) {
        reportMessage(
            file: file, lineNumber: lineNumber, content: content, example: example, category: .failure
        )
    }

    /// Print the message to stdout in a format Xcode can use.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    ///   - category: the ReportCategory for the message (e.g., .error, .warning, etc.)
    private func reportMessage(
        file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample?, category: ReportCategory
    ) {
        let error = ScenarioError(
            file: file, lineNumber: lineNumber, content: content, example: example, category: category
        )
        print(error.xcode)
    }
}
