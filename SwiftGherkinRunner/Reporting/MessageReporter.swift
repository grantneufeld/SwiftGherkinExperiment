import Foundation

/// Handles reporting of messages (errors, warnings, failures).
protocol MessageReporter {
    /// Report an error.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    func error(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample?)

    /// Report a warning.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    func warning(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample?)

    /// Report a test success.
    /// - Parameters:
    ///   - file: the FeatureFile the success occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the success applies to
    ///   - example: if running as part of an example, the GherkinExample
    func success(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample?)

    /// Report a test failure.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    func failure(file: FeatureFile, lineNumber: Int, content: String, example: GherkinExample?)
}
