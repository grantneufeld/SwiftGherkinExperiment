import Foundation

// swiftlint:disable force_try

/// Regular Expression extensions to String.
extension String {
    /// The UTF16 length (count) of the string.
    public var length: Int {
        utf16.count
    }

    /// Is the String a valid regular expression?
    public var isValidRegex: Bool {
        do {
            _ = try NSRegularExpression(pattern: self)
            return true
        } catch {
            return false
        }
    }

    /// Get the capture groups from within a string using a regular expression pattern.
    /// - Parameter pattern: a regular expression pattern String
    /// - Throws: if the pattern is invalid
    /// - Returns: an Array of Strings
    public func captureGroups(pattern: String) throws -> [String] {
        var result: [String] = []
        let regex = try NSRegularExpression(pattern: pattern)
        let range = NSRange(location: 0, length: length)
        let matches = regex.matches(in: self, range: range)
        for match in matches {
            let captureGroupsCount = match.numberOfRanges - 1
            if captureGroupsCount > 0 {
                for index in 1...captureGroupsCount {
                    let captureGroupMatchRange = match.range(at: index)
                    if let captureGroupRange = Range(captureGroupMatchRange, in: self) {
                        let captureGroupString = String(self[captureGroupRange])
                        result.append(captureGroupString)
                    }
                }
            }
        }
        return result
    }

    /// Replace any parts of the String that match the given regular expression with a new value.
    /// (`gsub` -> global substitution.)
    /// - Parameters:
    ///   - pattern: regular expression to search for
    ///   - with: the string to replace any matches with
    /// - Returns: the modified String
    public func gsub(pattern: String, with newValue: String) -> String {
        guard pattern.isValidRegex else {
            // make no changes if the regular expression is not valid
            return self
        }
        let regex = try! NSRegularExpression(pattern: pattern)
        let range = NSRange(location: 0, length: length)
        return regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: newValue)
    }

    /// Split the text into chunks, using a regular expression as the separator.
    /// - Parameters:
    ///   - pattern: regular expression String defining the separator
    ///   - limit: the maximum number of chunks to do (last chunk will be all remaining text).
    ///     No limit if `0` (default).
    /// - Returns: an Array of Strings
    public func split(pattern: String, limit: Int = 0) -> [String] {
        guard pattern.isValidRegex else {
            // cannot split the string if the regular expression is not valid
            return [self]
        }
        var chunks: [String] = []
        let regex = try! NSRegularExpression(pattern: pattern)
        let fullRange = NSRange(location: 0, length: length)
        let matches = regex.matches(in: self, options: [], range: fullRange)
        var nextStart = 0
        for match in matches {
            let range = match.range
            let chunkStart = nextStart
            let chunkEnd = range.location
            if chunkStart >= length {
                break
            }
            if chunkEnd >= length {
                fatalError("chunkEnd goes past the end of the text")
            }
            if chunkEnd > chunkStart {
                if let chunk = splitGetChunk(chunkStart: chunkStart, chunkEnd: chunkEnd) {
                    chunks.append(chunk)
                }
            }
            nextStart = range.location + range.length
        }
        if nextStart <= length {
            if let chunk = splitGetChunk(chunkStart: nextStart, chunkEnd: length) {
                chunks.append(chunk)
            }
        }
        return chunks
    }

    private func splitGetChunk(chunkStart: Int, chunkEnd: Int) -> String? {
        if chunkEnd >= chunkStart {
            let chunkRange = NSRange(location: chunkStart, length: chunkEnd - chunkStart)
            let chunkRangeText = Range(chunkRange, in: self)
            return String(self[chunkRangeText!])
        } else {
            return nil
        }
    }
}
