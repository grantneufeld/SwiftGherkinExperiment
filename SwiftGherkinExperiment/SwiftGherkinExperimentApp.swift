import SwiftUI

@main
struct SwiftGherkinExperimentApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
